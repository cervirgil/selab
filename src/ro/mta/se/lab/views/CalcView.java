package ro.mta.se.lab.views;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import ro.mta.se.lab.controllers.CalcController;
import ro.mta.se.lab.interfaces.OnModelChangedListener;
import ro.mta.se.lab.models.CalcOperations;

public class CalcView extends StackPane implements OnModelChangedListener {

    private CalcController controller;

    public CalcView(final CalcController controller) {
        super();

        this.controller = controller;
        this.controller.setModelChangedListener(this);

        Button btn = new Button();
        btn.setText("+");
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                //controller.doOperation(100, CalcOperations.ADD);
            }
        });

        getChildren().add(btn);
    }

    @Override
    public void onModelChanged(double value) {
        // TODO update text input
        System.out.println("New value " + value);
    }
}
