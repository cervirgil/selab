package ro.mta.se.lab.patterns;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * A logging class that implements both singleton and proxy patterns
 */
public class Logger implements Runnable {

    private static Logger instance;

    private LogType logLevel;

    private LogType currentLogLevel;

    private String currentMessage;

    private Logger() {
        this.logLevel = LogType.VERBOSE;
    }

    private static Logger getInstance(LogType logType, String message) {
        if (instance == null)
            instance = new Logger();

        instance.currentLogLevel = logType;
        instance.currentMessage = message;

        return instance;
    }

    @Override
    public void run() {
        if (currentLogLevel.level >= instance.logLevel.level) {
            // Do logging
            try {
                FileOutputStream outputStream = new FileOutputStream("log.txt", true);
                byte[] strToBytes = currentMessage.getBytes();
                outputStream.write(strToBytes);

                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean log(LogType logType, String message) {
        Logger instance = getInstance(logType, message);

        new Thread(instance).start();

        return false;
    }

    public enum LogType {
        VERBOSE(0),
        DEBUG(1),
        ERROR(2),
        RELEASE(3);

        int level;

        LogType(int lovel) {
            this.level = level;
        }
    }
}
