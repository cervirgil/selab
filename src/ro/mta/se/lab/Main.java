package ro.mta.se.lab;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ro.mta.se.lab.controllers.CalcController;
import ro.mta.se.lab.models.CalcModel;
import ro.mta.se.lab.views.CalcView;

import java.net.URL;

public class Main extends Application{

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Load Model
        CalcModel model = new CalcModel();

        // Init Controller
        CalcController controller = new CalcController(model);

        URL res =  Main.class.getClassLoader().getResource("views/calc_main.fxml");
        FXMLLoader loader = new FXMLLoader(res);
        loader.setController(controller);
        Parent root = loader.load();
        Scene scene = new Scene(root);

        // Show view
        primaryStage.setTitle("CalcMVC");
        // primaryStage.setScene(new Scene(new CalcView(controller), 200, 400));
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }
}