package ro.mta.se.lab.models;

public class Student {

    private int average = 2;

    public Student(int average) {
        this.average = average;
    }

    public int getAverage() {
        return average;
    }

    public void setAverage(int average) {
        this.average = average;
    }
}