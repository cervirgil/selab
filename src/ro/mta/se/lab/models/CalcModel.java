package ro.mta.se.lab.models;

import ro.mta.se.lab.interfaces.OnModelChangedListener;

public class CalcModel {

    private static final String TAG = String.format("[%s] : ", CalcModel.class.getSimpleName());

    private double currentValue;

    private OnModelChangedListener listener;

    public CalcModel() {
        this.currentValue = 0;
    }

    public double getValue() {
        return currentValue;
    }

    public void setValue(double currentValue) {
        this.currentValue = currentValue;
        System.out.println(TAG + "new value " + this.currentValue);
        if (this.listener != null) {
            this.listener.onModelChanged(this.currentValue);
        }
    }

    public void setOnChangedListener(OnModelChangedListener listener) {
        this.listener = listener;
    }
}
