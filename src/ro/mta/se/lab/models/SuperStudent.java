package ro.mta.se.lab.models;

import ro.mta.se.lab.interfaces.Mammal;
import ro.mta.se.lab.interfaces.Thinker;

public class SuperStudent extends Student implements Mammal, Thinker {

    public SuperStudent(int average) {
        super(average);
    }

    @Override
    public void walk() {

    }

    @Override
    public void think() {

    }
}
