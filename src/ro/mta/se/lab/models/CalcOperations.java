package ro.mta.se.lab.models;

public enum CalcOperations {
    ADD, SUBS, MULTIPLY, DIVIDE, EQUAL;
}
