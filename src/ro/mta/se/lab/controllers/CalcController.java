package ro.mta.se.lab.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBoxBuilder;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;
import ro.mta.se.lab.Main;
import ro.mta.se.lab.interfaces.OnModelChangedListener;
import ro.mta.se.lab.models.CalcModel;
import ro.mta.se.lab.models.CalcOperations;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CalcController implements Initializable {

    private static final String TAG = String.format("[%s] : ", CalcController.class.getSimpleName());

    private CalcModel calcModel;

    @FXML
    private TextField calcText;

    @FXML
    private Button plusButton;

    @FXML
    private Button minusButton;

    @FXML
    private Button multiplyButton;

    @FXML
    private Button divideButton;

    @FXML
    private Button equalsButton;

    public CalcController(CalcModel calcModel) {
        this.calcModel = calcModel;
    }

    private void doOperation(double operand, CalcOperations operation) {
        switch (operation) {
            case ADD:
                calcModel.setValue(calcModel.getValue() + operand);
                break;
            case SUBS:
                calcModel.setValue(calcModel.getValue() - operand);
                break;
            case MULTIPLY:
                calcModel.setValue(calcModel.getValue() * operand);
                break;
            case DIVIDE:
                calcModel.setValue(calcModel.getValue() / operand);
                break;
            case EQUAL:
                break;
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rs) {
        assert plusButton != null : "fx:id=\"plusButton\" was not injected: check your FXML file 'calc_main.fxml'.";
        assert minusButton != null : "fx:id=\"minusButton\" was not injected: check your FXML file 'calc_main.fxml'.";
        assert multiplyButton != null : "fx:id=\"multiplyButton\" was not injected: check your FXML file 'calc_main.fxml'.";
        assert divideButton != null : "fx:id=\"divideButton\" was not injected: check your FXML file 'calc_main.fxml'.";
        assert equalsButton != null : "fx:id=\"equalsButton\" was not injected: check your FXML file 'calc_main.fxml'.";
        assert calcModel != null : "fx:id=\"calcModel\" was not injected: check your FXML file 'calc_main.fxml'.";

        initButtonListeners();
    }

    private void initButtonListeners() {
        plusButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Double value = validateInput();
                if (value != null)
                    doOperation(value, CalcOperations.ADD);

                event.consume();
            }
        });

        minusButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Double value = validateInput();
                if (value != null)
                    doOperation(value, CalcOperations.SUBS);

                event.consume();
            }
        });

        multiplyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Double value = validateInput();
                if (value != null)
                    doOperation(value, CalcOperations.MULTIPLY);

                event.consume();
            }
        });

        divideButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Double value = validateInput();
                if (value != null)
                    doOperation(value, CalcOperations.DIVIDE);

                event.consume();
            }
        });

        equalsButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                calcText.setText(String.format("%s", calcModel.getValue()));
                event.consume();
            }
        });
    }

    private Double validateInput() {
        try {
            return Double.parseDouble(calcText.getText());
        } catch (NumberFormatException e) {
            System.out.println(TAG + e.getMessage());
            Stage popupWindow = new Stage();
            popupWindow.initModality(Modality.WINDOW_MODAL);

            PopupController controller = new PopupController(e.getMessage(), PopupController.PopupType.ERROR, popupWindow);
            URL res =  CalcController.class.getClassLoader().getResource("views/popup.fxml");
            FXMLLoader loader = new FXMLLoader(res);
            loader.setController(controller);

            try {
                popupWindow.setScene(new Scene((Parent) loader.load()));
                popupWindow.show();
            } catch (IOException e1) {
                System.out.println(TAG + "popup error: " + e1.getMessage());
            }
            return null;
        }
    }

    public void setModelChangedListener(OnModelChangedListener listener) {
        calcModel.setOnChangedListener(listener);
    }
}
