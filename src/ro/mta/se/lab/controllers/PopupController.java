package ro.mta.se.lab.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class PopupController implements Initializable {

    @FXML
    private Text messageText;

    @FXML
    private Button discardButton;

    private String message;

    private PopupType type;

    private Stage popup;

    public PopupController(String message, PopupType type, Stage popup) {
        this.message = message;
        this.type = type;
        this.popup = popup;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        assert messageText != null : "fx:id=\"messageText\" was not injected: check your FXML file 'popup.fxml'.";
        assert discardButton != null : "fx:id=\"discardButton\" was not injected: check your FXML file 'popup.fxml'.";

        initView();
    }

    private void initView() {
        popup.centerOnScreen();
        messageText.setText(message);
        messageText.getStyleClass().clear();
        messageText.getStyleClass().add(type == PopupType.ERROR ? "error" : "info");

        discardButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               popup.close();
            }
        });
    }

    public enum PopupType {
        ERROR, INFO;
    }
}
