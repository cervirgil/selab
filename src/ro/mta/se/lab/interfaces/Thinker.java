package ro.mta.se.lab.interfaces;

public interface Thinker {

    void think();

    void walk();
}
