package ro.mta.se.lab.interfaces;

public interface Mammal {

    void walk();
}
