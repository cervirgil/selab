package ro.mta.se.lab.interfaces;

public interface OnModelChangedListener {

    void onModelChanged(double value);
}
