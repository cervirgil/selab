package ro.mta.se.lab.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import ro.mta.se.lab.interfaces.OnModelChangedListener;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class CalcModelTest {
    private CalcModel instance;

    @Mock
    private OnModelChangedListener mockListener = Mockito.mock(OnModelChangedListener.class);

    @BeforeEach
    void setUp() {
        instance = new CalcModel();
        instance.setOnChangedListener(mockListener);
    }

    @Test
    void getValue() {
        assertEquals(instance.getValue(), 0);
    }

    @Test
    void setValue() {
        assertEquals(instance.getValue(), 0);
        instance.setValue(20);

        // Verify that onModelChanged was called
        verify(mockListener, times(1)).onModelChanged(20);

        assertNotEquals(instance.getValue(), 10);
        assertEquals(instance.getValue(), 20);
    }

    @Test
    void setOnChangedListener() {
    }

    @Test
    void nullInstance() {

    }
}